peergoggles
===========

Outbound bandwidth monitoring, congestion avoidance and bufferbloat management for systems running heavy uploading apps. Peergoggles uses iptables, but it can be queried by other programs in order to make decisions or control non-iptables firewalls.

**This is pre-alpha non-functioning code. This line will be removed and replaced with instructions once there is something to alpha test.**

## The problem

Have you ever run an app that uploaded a ton of data as fast as possible? BitTorrent is a great candidate, but this could also be as simple as uploading 100 full-size photos to Flickr. Did you notice that even though you're *uploading,* your *download* speeds and page load times get terrible as well? What's the deal? Meet **bufferbloat.**

## Where peergoggles comes in

Peergoggles is designed to work in concert with programs and computers which upload a lot of data and want to avoid causing bufferbloat-related congestion.

### Observation
* Peergoggles observes bandwidth availability in the long term (hours to weeks)
* Peergoggles observes congestion in the short term (milliseconds to seconds)

### Action
* Peergoggles provides "safe ceiling" numbers for upload bandwidth - the maximum rate a program or computer should ever even *try* to send - based on long-term observation
* Peergoggles can throttle send rates directly on systems with iptables to reduce send rate if it detects congestion
* Peergoggles can advise other applications to throttle their send rates if iptables is not available or desired

**Peergoggles is meant to help any heavy-upload app which wants to be as unobtrusive as possible, and which can stand to be throttled unpredictably.**

### Examples plz

Much of the data you send over the Internet travels over TCP/IP. We're interested in the [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) part here, because a series of unfortunate choices by broadband equipment manufacturers (bufferbloat), when combined with TCP, can cause *upload* congestion to ruin *download* speeds.

Let's say you visit http://dailykitten.com/ and click on a picture of a kitten to see the high-resolution version.  TCP breaks the image (kitten.jpg) into little pieces (packets) and sends them out on their journey through the wilds of the Internet from the Daily Kitten server to your browser.

Because the internet is *packet switched,* unlike the old telephone network which is *circuit switched,* even though those packets are sent out in order - from the top of kitten.jpg to the bottom - and even though they're only sent out through one "tube" - the one that connects dailykitten.com to its ISP - after that, the packets may take multiple different routes between Daily Kitten and you! These can change in fractions of a second, based on which routes the computers in the middle think is most efficient. The important part is that this means that single packets - pieces of kitten.jpg - can arrive out of order, or even not arrive at all if there was an overload somewhere.

TCP is built to restore order from chaos. The inventors of the Internet, while perhaps not anticipating Daily Kitten, did anticipate that users would want to receive their files in the correct order and without pieces missing. Thus, TCP.

* **ORDERING:** TCP ensures that if packets arrive out of order, they are assembled in the correct order before being presented to the program which asked for kitten.jpg.
* **COMPLETENESS:** TCP ensures that if a packet is lost, it will be re-requested from the sender in a way completely transparent to the program which asked for kitten.jpg.
* **CONGESTION CONTROL:** TCP also ensures that if a link between Daily Kitten and you becomes overloaded, the sender will reduce their transmission rate in order to alleviate congestion for everyone.

Let's dig a little deeper into TCP to examine where problems can happen.

#### An overview of TCP

How does TCP know that packets arrived in the wrong order, or got lost on their way to their destination? Well, every so often the destination (your computer) sends a special packet back to the sender (Daily Kitten) called an ACK (for acknowledgement).  Much of TCP's goodness - its ability to deliver kitten.jpg intact and in order, *and* its ability to deliver kitten.jpg quickly but without overwhelming either your computer or the ones in between Daily Kitten and you - depends on these ACK packets.

As Daily Kitten sends kitten.jpg, it sends the packets out in order: 1, 2, 3, 4, 5... and every so often, your computer says "hey, I got everything up to packet 5!" in an ACK packet. But what if a packet got lost? Daily Kitten sent 1, 2, 3, 4, and 5, and then decides to wait for an ACK because 5 packets is enough (this is a simplification, but it basically works this way). But packets 4 and 5 got eaten by a grue on the way. Eventually, seeing no more kitten.jpg packets from Daily Kitten, your computer infers that it must be waiting for an ACK, so it says "hey, I got everything up to packet 3!"

This mechanism - the ACK which says "I got everything up to..." - is combined with countdown timers to keep TCP reliable. (I haven't fact-checked all of this, I'm happy to have a pull request if I'm wrong on details here BTW.)

The sender (Daily Kitten) keeps a countdown timer which is reset every time it gets an ACK from you. If no ACK appears, Daily Kitten stops sending and starts another timer. If the ACK arrives, it starts sending more packets again. If the timer expires, Daily Kitten gives up, assumes everything it sent was lost and retransmits it.

Likewise, the receiver (you) starts a timer when it receives a packet.  Normally receivers only send ACKs every few packets so as not to waste bandwidth.  But there are many reasons why a packet flow might stop for a while.  If the receiver figured it'd wait to send an ACK until it saw packet 5, but it only gets packets 1, 2 and 3, eventually its timer will expire and it will send an early ACK to Daily Kitten.

It's kind of important to remember that TCP doesn't know anything about kittens or JPGs or web sites.  It just sends data.  So it could be sending chat packets - where pauses are normal - or it could be sending large JPGs of kittens, where pauses aren't so normal.  The ACK and timers on each end make TCP really flexible for sending streams of lots of little packets that pause a lot (chat) or lots of max-size packets that don't often pause (big JPGs).

#### How do packets get lost?

This might seem like a strange thing to explain at this point, but it turns out it's important.  As kitten.jpg flows from Daily Kitten to your computer, any number of things can happen to the packets containing kitten.jpg. *Usually* they all show up intact, mostly in order, and TCP reassembles them bit by bit and feeds the data to whatever program that requested it - in this case, your browser.

But sometimes, a packet really *does* get eaten by a grue. (That's a technical term for "it didn't arrive and we have no explanation.") For example, a cosmic ray might hit one of the computers in the middle, flip a bit in its memory, and it forgets to send a few packets. One of those packets that was forgotten was packet 4 of kitten.jpg. This is an actual real thing - cosmic rays *do* hit computers and *do* flip bits in their memory. (This is one reason the Mars rovers, the Cassini probe at Saturn, and so on always use computer technology that seems truly *ancient* by modern standards - it's because the chips they use, which are often functionally identical to the ones in common PCs - have to be specially built to be extremely resistant to cosmic rays. On Earth, our atmosphere protects us from many.)

Or, maybe a power surge might briefly affect one of the tubes - er, wires - between Daily Kitten and you, causing it to lose a few packets and not be able to recover them, one of which happened to be unlucky packet 4 of kitten.jpg.

Or, a crew with a backhoe in Akron, Ohio cuts a fiber *just* as unlucky packet 4 was firing across it.

These events are pretty rare, though.  The biggest reason for packets to disappear on their way to their destination is *network congestion.*

#### What happens during network congestion?

The inventors of TCP were pretty smart.  They recognized that cosmic rays and incautious backhoe users would happen, though they probably had never dreamed of fiber-optic cable back then!  So, they built TCP with a method for assuring lost packets would be retransmitted.

But even then, back when pterodactyls roamed the skies, *data transmission was actually pretty reliable.*  Most of the time, packets got where they were going.  However, back then, data transmission was *really slow.* And when I say slow, I mean SUH-LOWWWWW. When I got on the Internet in middle school, it wasn't a household word yet, people didn't dial up to anything except pre-Internet AOL and Compuserve. I used a 1200 baud (1.2K) modem to access the Internet. You may remember the term "56K" - by the late 90s, dialup modems were capable of 56Kbps speed (sort of like 56000 baud). But when I got online just a few years before that, an *entire small university* would often be connected to the Internet by a single 56K line for several thousand students.

This had always been a problem, ever since TCP was invented (and then fought about and partly reinvented to make it work better).

The way it goes is, as people get on the Internet for the first time, they quickly realize just *how freaking cool* it is, and start sending tons and tons of data.  Pretty soon, the computers in the middle between point A and point B got overloaded.  They were constantly slammed with traffic.  They couldn't upgrade quickly enough, the technology wasn't fast enough, and it was really expensive.  What to do?

The inventors of TCP - which as you may be realizing, wasn't invented in a "Eureka!" moment but over a period of years of experimenting and tweaking and tuning - realized they needed *congestion control.*  Somehow, TCP should detect when a link becomes congested, and slow down or stop sending data for a while.  If this didn't happen, and *soon,* episodes of "congestion collapse" would become the norm - once congestion becomes bad enough, everyone trying to retransmit their data causes the entire network to collapse and not send *any* data for *anybody.*

There were a lot of schemes proposed for congestion control, but one of the goals of TCP was to make it a "dumb" protocol. It shouldn't know anything about what it was transmitting, or where, or why, and there are good reasons for that.  And so the pioneers hit on something.  If a computer is overloaded with packets, it could just *throw them away* to prevent itself from crashing.  TCP already had a way of recovering from this - it would figure out that a packet was lost, and eventually retransmit.

"But wait!" you might say, "that doesn't solve the congestion collapse problem. Everybody would still be retransmitting their 'lost' packets, and nothing is solved at all!"

That's right. And so the *congestion avoidance* part of TCP was born. Here's the bottom line of alllllll of the above history.

**Every time a packet is lost, TCP assumes it is because of congestion, and it slows down its send rate to be a good neighbor.**  After all, cosmic rays and incautious backhoe users are rarer than congestion - it's a pretty safe bet.  The inventors combined congestion avoidance with "fast recovery" and thus catastrophe was avoided, and decades of tweaks to just how all this works in TCP continued.

#### Cool story, bro. So about that bufferbloat thing?

I'm getting there.  Turns out, another minor disaster was in the making at the start of the 21st century, as broadband Internet access started sweeping the globe.  In many countries, broadband was and is "asymmetric."  Users are expected to download much more than they upload.  So, in America in 2000, you might get an ADSL link ("A" for "Asymmetric") with 128K upload and 1024K download capacity.

Even before broadband, *all* such devices had a "buffer" - a bit of memory that could store incoming and outgoing data in case the network, or the computer, or whatever was not ready to accept it yet.  On a 56K modem, the 16550 chip had a buffer of 16 *bytes.*  The modem could store 16 bytes, waiting for the computer to read them, before it had to stop accepting more from the telephone line.

Memory and chip prices were falling like a rock back then. And broadband manufacturers were turning out generic chips for DSL and cable modems that would be used in many countries, many of them with far better Internet service than the United States, which still ranks low on the list even today.  In South Korea, maybe you could get DSL with 1024K upload and 8192K download.  The faster the speed, the bigger the buffer needs to be in order to prevent needless data loss when some other part of the modem or computer or local network is temporarily busy.

But what about those pitiful countries where 128K upload and 1024K download were still the norm?  Manufacturers aren't going to reduce the amount of buffer, they're going to sell the same chips to everyone whenever possible, and just run them at different speeds.  That's one beginning of bufferbloat.

The other is even more facepalmy. Memory and chip prices, as I said, were plummeting. And packet loss is bad for TCP, or so people had begun to think. After all, it makes TCP stop everything and then slow itself way down for a while! We don't *want* to throw those packets away, said the broadband manufacturers, because then our product might seem slow during congestion.  And buffer memory is cheap.  Let's make the buffers *even bigger.*

#### The unintended consequences

Remember how every once in a while, the receiver of TCP packets sends an ACK packet to acknowledge it got what the sender sent?

What happens if you have more than one program running on your computer, or on your network?  To bring it back to the present day, what if you're watching Netflix and your spouse is uploading the photo album from your last vacation to Flickr?

It's important to know that telecom speeds are *still* too slow, just like they always have been.  Everything on the wifi at your home, and the network at your ISP, is faster by *far* than the wire in between.  This has been the case since the 1970s and shows no signs of getting fixed any time soon.  Sending data over long wires is harder than sending it over short wires - and worse, some technically innovative countries like the United States are stuck with a slow, outdated, underinvested broadband network due to lack of competition between big ISPs.

The reason this matters is because your spouse's computer can pump photos up to Flickr far faster than your cable modem can send them, and Netflix can pump movies down to your PS3 far faster than your cable modem can receive them.  As you watch a movie on Netflix, it watches the TCP congestion control and makes educated guesses about how much data it can send to you over the actual wire to your house. Eventually it does a pretty good job of figuring out how fast your connection is, and delivers you the best quality it can.

So you're streaming Gigli from Netflix, and every so often, your Netflix box sends an ACK back to Netflix saying "hey, got the one scene that was really bad, send the next!"

**What happens when your spouse starts a huge upload to Flickr?**

Because the computer is so much faster than the modem, it sends the first couple photos seemingly instantly.  Have you ever noticed that?  When you upload something, it transfers a whole bunch really fast in 2 seconds and then seems to pause, and then slow down to the actual speed which you know you paid for (hopefully)? *That's a symptom of the buffer on your cable modem being filled.*

So your spouse's computer is shoveling photos at the modem as fast as it can take it, which locally is *much* faster than it can send it to your ISP, and the buffer fills up almost instantly.  But you're trying to watch Netflix - a downloading activity - but you have to send ACKs back to Netflix every so often to confirm that you got the last 3 seconds of Gigli.

Once the buffer on your modem is full of vacation photos going to Flickr, your ACKs go in as soon as they're sent, but then *the ACKs have to wait in the buffer* to be dribbled out over the slow wire to your ISP before they're actually sent.

Now you may remember the part about countdown timers, and about dropped packets causing TCP to say "whoa! probably congestion! I should slow down." The ACK your Netflix app sent might spend two, five, eight seconds in your modem's buffer. That's an *eternity* in TCP time. The countdown timer at Netflix might expire, and it considers your ACK to be lost.  Or it might start getting ACKs from eight seconds ago and decide you're eight seconds behind where it was sending, so clearly your line is slower (even though all the data is getting through), so it better drop the quality before the stream crashes.

Oopsie.

This is how *uploading* tons of data over poorly-engineered consumer broadband gear - which is a lot of it - leads to horrible speeds and bad lag at *downloading* on the same wire.

Forget ACKs for a second.  What if you're just browsing the web or checking email?  That's not streaming, so it shouldn't be affected, right?  Wrong.  When you make a connection to a web site or an email server, TCP has to open that connection, and it does so using a "three-way handshake." You send a packet to the server, the server acknowledges, you acknowledge the acknowledgement, and then data starts to flow.  Just try browsing TMZ when your two outbound packets have to sit in a buffer for five seconds before even making it out of your house.

#### Well crap. What to do?

There's a lot of work being done on this. A number of broadband equipment manufacturers met recently and agreed to stop putting giant buffers in their modems.  That will help, but unfortunately - and especially in laggard countries like the United States where 4096K download is still considered, for the purposes of regulation, to be "broadband" - a lot of this equipment is out there, it has no moving parts, and it's going to stay in service for years and in some cases decades.  We can't do much about that.

There are also yet more tweaks to TCP with this in mind.  [TCP CoDel](https://en.wikipedia.org/wiki/CoDel) is a new variant of congestion control for TCP which takes bufferbloat into account and does the best it can even when stuck behind devices with giant buffers.  It's not widespread yet, but it's gaining a lot of interest.

But the best thing to do for now, especially when stuck with broadband gear manufactured before, say, 2014 (and some bufferbloat gear is still coming out today), is to *limit your send rate to avoid filling up the buffer in the first place.*  That's where peergoggles comes in.

## How peergoggles helps

Peergoggles can't know if it's on a computer with TCP CoDel installed. It can't know if it's behind a brand new fancy cable modem with no extra buffer, or something the phone company gave you built in 2010 which can support 4096K upload, but you're two miles out of town and only have 256K upload.

What Peergoggles *can* do is observe bandwidth conditions over time, and directly limit send rates on a computer (using iptables on Linux), or provide advice to other programs which wish to limit send rates in some other way.

Peergoggles is concerned with measuring and regulating bandwidth on two time scales: hours to weeks (long-term), and milliseconds to seconds (short-term).  Long-term measurement gives us an idea of how much bandwidth is available on average when everything is great and there's no congestion, and how much that long-term number tends to fluctuate.  Short-term measurement tells us if we might be congested *right now* and if so, to throttle back our send rate.

**The point of peergoggles is to help unattended systems which upload a lot of data to avoid causing bufferbloat congestion on their link to the Internet.**

While it could have many uses, the main idea is to prevent normally-ignored systems like BitTorrent boxes, Tor relays, or WAN file servers from ruining your Netflix or browsing experience by triggering bufferbloat congestion.

Peergoggles itself is under the umbrella of [The Cipollini Project](https://github.com/gordon-morehouse/cipollini), which is a collection of software to make deployment of unattended, "plug-and-forget" relays for the Tor Network easy.  Peergoggles is important in this application because if the relay starts messing with people's Netflix due to bufferbloat, it will not be forgotten, and is likely to get unplugged.

**Peergoggles is meant to help any heavy-upload app which wants to be as unobtrusive as possible, and which can stand to be throttled unpredictably.**
